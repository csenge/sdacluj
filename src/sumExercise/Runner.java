package sumExercise;

import java.util.ArrayList;
import java.util.List;

public class Runner {


    public static void main2(String[] args) {
      SumCalculator sumCalculator = new SumCalculator();

      // test scenario 1.
        List<Object> scenario1 = new ArrayList<>();
        scenario1.add("12");
        scenario1.add("1as");
        scenario1.add(2);
        scenario1.add("5");
        scenario1.add(5);

        int resultat1 = sumCalculator.calculateSum(scenario1);
        System.out.println("Result is: " + resultat1);

      // test scenario 1.
      List<Object> scenario2 = new ArrayList<>();
      scenario2.add("12");
      scenario2.add("1");
      scenario2.add(2);
      scenario2.add("5");
      scenario2.add(5);
      scenario2.add(new Customer("Ion", 20));

      int rezultatscenario2 = sumCalculator.calculateSum(scenario2);
      System.out.println("Result is: " + rezultatscenario2);
    }

    public static void main(String[] args) {
      String s = "abc";

      s.concat("def");

      System.out.println(s); // result will be abc because there is no reassigning and String is immutable

      s = s.concat("xyz");

      System.out.println(s); // s will be updated because of the reassignment

    }

}
