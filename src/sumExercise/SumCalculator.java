package sumExercise;

import java.util.List;

public class SumCalculator {

    public int calculateSum(List<Object> listValues) {
        int sum = 0;

        for (Object currentObject: listValues) {
            try {

                sum += Integer.parseInt(String.valueOf(currentObject));

                String i = String.valueOf(new Integer(10));
//                sum += (int) currentObject;
            } catch (Exception e) {
                System.out.println("Exception occurred for: " + currentObject);
                System.out.println("Exception details: " + e.getMessage());
            }
        }

        //todo insert addition logic
        return sum;
    }
}
